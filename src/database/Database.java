package database;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import libararyproject.LibararyProject;

public class Database {

    static String url = "jdbc:sqlite:./db/library.db";
    static Connection conn = null;

    public static Connection connect() {
        if (conn != null) {
            return conn;
        }
        try {
            conn = DriverManager.getConnection(url);
            System.out.println("Connected Database");
            return conn;
        } catch (SQLException ex) {
            Logger.getLogger(LibararyProject.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }

    public static void close() {
        try {
            if (conn != null) {
                conn.close();
                System.out.println("Closed Dabase");
                conn = null;
            }
        } catch (SQLException ex) {
            Logger.getLogger(LibararyProject.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}
