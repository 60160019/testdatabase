/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package database;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import libararyproject.LibararyProject;

/**
 *
 * @author informatics
 */
public class userDAO {

    public static boolean insert(User u) {
        Connection conn = Database.connect();
        try {
            Statement stm = conn.createStatement();
            String sql = "INSERT INTO user (\n"
                    + "                     loginName,\n"
                    + "                     password,\n"
                    + "                     name,\n"
                    + "                     surname,\n"
                    + "                     typeId\n"
                    + "                 )\n"
                    + "                 VALUES (\n"
                    + "                     '%s',\n"
                    + "                     '%s',\n"
                    + "                     '%s',\n"
                    + "                     '%s',\n"
                    + "                     '%d'\n"
                    + "                 );";
            stm.execute(String.format(sql, u.getLoginName(), u.getPassword(), u.getName(), u.getSurname(), u.getTypeId()));
            Database.close();
            return true;
        } catch (SQLException ex) {
            Logger.getLogger(userDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return true;
    }

    public static boolean update(User u) {
        String sql = "UPDATE user SET \n"
                + " loginName = '%s',\n"
                + " password = '%s',\n"
                + " name = '%s',\n"
                + " surname = '%s',\n"
                + " typeId = %d\n"
                + "WHERE userId = %d;";
        Connection conn = Database.connect();
        Statement stm;
        try {
            stm = conn.createStatement();
            stm.execute(String.format(sql, u.getLoginName(), u.getPassword(), u.getName(), u.getSurname(), u.getTypeId(), u.getUserId()));
            Database.close();
            return true;
        } catch (SQLException ex) {
            Logger.getLogger(userDAO.class.getName()).log(Level.SEVERE, null, ex);
        }

        Database.close();
        return true;
    }

    public static boolean delete(User u) {
        String sql = "DELETE FROM user WHERE userId = %d;";
        Connection conn = Database.connect();
        try {
            Statement stm = conn.createStatement();
            boolean ret = stm.execute(String.format(sql, u.getUserId()));
            Database.close();
            return ret;
        } catch (SQLException ex) {
            Logger.getLogger(userDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        Database.close();
        return true;
    }

    public static User getUser(int userId) {
        Connection conn = Database.connect();
        String sql = "SELECT * FROM user WHERE userId = %d";
        try {
            Statement stm = conn.createStatement();
            ResultSet rs = stm.executeQuery(String.format(sql, userId));
            if (rs.next()) {
                User user = toObject(rs);
                Database.close();
                return user;
            }
        } catch (SQLException ex) {
            Logger.getLogger(userDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        Database.close();
        return null;
    }

    public static ArrayList<User> getUsers() {
        ArrayList<User> list = new ArrayList<User>();
        Connection conn = Database.connect();
        try {
            Statement stm = conn.createStatement();
            String sql = "SELECT userId,\n"
                    + "       loginName,\n"
                    + "       password,\n"
                    + "       name,\n"
                    + "       surname,\n"
                    + "       typeId\n"
                    + "  FROM user\n"
                    + " WHERE password = 'password';";
            ResultSet rs = stm.executeQuery(sql);
            while (rs.next()) {
                User user = toObject(rs);
                list.add(user);
            }
            Database.close();
            return list;
        } catch (SQLException ex) {
            Logger.getLogger(LibararyProject.class.getName()).log(Level.SEVERE, null, ex);
        }

        return null;
    }

    private static User toObject(ResultSet rs) throws SQLException {
        User user;
        user = new User();
        user.setUserId(rs.getInt("userId"));
        user.setLoginName(rs.getString("loginName"));
        user.setPassword(rs.getString("password"));
        user.setName(rs.getString("name"));
        user.setSurname(rs.getString("surname"));
        user.setTypeId(rs.getInt("typeId"));
        return user;
    }
}
